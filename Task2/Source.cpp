//#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <stdlib.h>
#include <math.h>
#include <process.h>
#include <vector>
#include <fstream>

const int WORLD_SIZE = 10;
const char DEAD_SIGN = '.';
const char ALIVE_SIGN = '*';

using namespace std;

class Cell
{
public:
	int x, y;
//	bool state;
};
class Ground
{
private:
	bool field[WORLD_SIZE][WORLD_SIZE];
public:
	Ground();
	void reset();
	bool is_empty();
	bool GetCell(int x, int y);
	void SetCell(int x, int y, bool cell);
	Ground(Ground &d);
	Ground& operator=(Ground &d);
	bool operator==(Ground &d);
};
Ground::Ground(Ground &d)
{
	for (int i = 0; i < WORLD_SIZE; i++)
		for (int j = 0; j < WORLD_SIZE; j++)
			field[i][j] = d.field[i][j];
}

Ground& Ground::operator=(Ground &d)
{
	for (int i = 0; i < WORLD_SIZE; i++)
		for (int j = 0; j < WORLD_SIZE; j++)
			field[i][j] = d.field[i][j];
	return *this;
}
bool Ground::operator==(Ground &d)
{
	for (int i = 0; i < WORLD_SIZE; i++)
		for (int j = 0; j < WORLD_SIZE; j++)
			if (field[i][j] != d.field[i][j]) return false;
	return true;
}
void Ground::reset()
{
	for (int i = 0; i < WORLD_SIZE; i++)
		for (int j = 0; j < WORLD_SIZE; j++)
			field[i][j] = false;
}

bool Ground::is_empty()
{
	for (int i = 0; i < WORLD_SIZE; i++)
		for (int j = 0; j < WORLD_SIZE; j++)
			if (field[i][j]) return false;
	return true;
}

bool Ground::GetCell(int x, int y)
{
	if ((x < 0) || (x >= WORLD_SIZE) || (y < 0) || (y >= WORLD_SIZE)) return false;
	return field[x][y];
}

void Ground::SetCell(int x, int y, bool cell)
{
	if ((x < 0) || (x >= WORLD_SIZE) || (y < 0) || (y >= WORLD_SIZE)) return;
	field[x][y] = cell;
}
Ground::Ground()
{
	reset();
}

typedef vector<Cell> CellList;

class LifeWorld
{
private:
	int step;
	bool end_of_game;
	Ground curr_ground;
	vector <CellList> WorldState;
	void make_step();
public:
	void Draw();
	bool is_end_of_game();
	void reset();
	void setXY(string str);
	void clearXY(string str);
	void stepN(string str);
	void back();
	void save(string str);
	void load(string str);
	LifeWorld();
	~LifeWorld();
};

void LifeWorld::Draw()
{
	system("cls");
	cout << " ABCDEFGHIJ " << endl;
	for (int i = 0; i < WORLD_SIZE; i++)
	{
		cout << WORLD_SIZE - 1 - i;
		for (int j = 0; j < WORLD_SIZE; j++)
			cout << (curr_ground.GetCell(j, WORLD_SIZE - 1 - i) ? ALIVE_SIGN : DEAD_SIGN);
		cout << WORLD_SIZE - 1 - i << endl;
	}
	cout << " ABCDEFGHIJ " << endl;
}

bool LifeWorld::is_end_of_game()
{
	return end_of_game;
}
void LifeWorld::reset()
{
	step = 0;
	end_of_game = false;
	curr_ground.reset();
	WorldState.clear();
}

void LifeWorld::setXY(string str)
{
	if (str.length() != 2) return;
	char X = str.at(0);
	char Y = str.at(1);
	if ((X < 'A') || (X > 'J') || (Y < '0') || (Y > '9')) return;
	curr_ground.SetCell((int)(X - 'A'), (int)(Y - '0'), true);
}

void LifeWorld::clearXY(string str)
{
	if (str.length() != 2) return;
	char X = str.at(0);
	char Y = str.at(1);
	if ((X < 'A') || (X > 'J') || (Y < '0') || (Y > '9')) return;
	curr_ground.SetCell((int)(X - 'A'), (int)(Y - '0'), false);
}

void LifeWorld::stepN(string str)
{
	if (str.length() == 0) return;
	int N = atoi(str.c_str());
	for (int i = 0; i < N ; i++)
	{
		if (end_of_game) return;
		make_step();
	}
}

void LifeWorld::make_step()
{
	// � WorldState ��������� ����� ����� ������ �� curr_ground;
	int i, j;
	CellList l;
	for (i = 0; i < WORLD_SIZE; i++)
		for (j = 0; j < WORLD_SIZE; j++)
			if (curr_ground.GetCell(i, j))
			{
				Cell c;
				c.x = i;
				c.y = j;
				l.push_back(c);
			}
	WorldState.push_back(l);
	// ������� ������ ������ Ground (new_ground)
	Ground new_ground;
	// � ��� � ����� 10x10 ���� �������� ������� �� curr_ground ������� �������� �� 1 ��� (������ ��������������!)
	for (i = 0; i < WORLD_SIZE; i++)
	{
		for (j = 0; j < WORLD_SIZE; j++)
		{
			int alive_count = 0;
			if (curr_ground.GetCell((i - 1) % 10, (j - 1) % 10)) alive_count++;
			if (curr_ground.GetCell((i - 1) % 10, j % 10)) alive_count++;
			if (curr_ground.GetCell(i % 10, (j - 1) % 10)) alive_count++;
			if (curr_ground.GetCell((i + 1) % 10, (j - 1) % 10)) alive_count++;
			if (curr_ground.GetCell((i - 1) % 10, (j + 1) % 10)) alive_count++;
			if (curr_ground.GetCell((i + 1) % 10, (j + 1) % 10)) alive_count++;
			if (curr_ground.GetCell((i + 1) % 10, j % 10)) alive_count++;
			if (curr_ground.GetCell(i % 10, (j + 1) % 10)) alive_count++;
			if (curr_ground.GetCell(i, j))
			{
				if ((alive_count == 2) || (alive_count == 3))
					new_ground.SetCell(i, j, true);
			}
			else
			{
				if (alive_count == 3)
					new_ground.SetCell(i, j, true);
			}
		}
	}
	// ��������� ��� new_ground �� ������ (� �� ����� ����)
	// ��������� ��� new_ground �� ����� curr_ground (� �� ����� ����)
	if (new_ground.is_empty() || (new_ground == curr_ground))
		end_of_game = true;
	// ������ �� new_ground ��������� � curr_ground
	curr_ground = new_ground;
	// step++
	step++;
}

void LifeWorld::back()
{
	if (step > 0)
	{
		curr_ground.reset();
		CellList l;
		l = WorldState.back();
		for (CellList::iterator I = l.begin(); I < l.end(); I++)
			curr_ground.SetCell(I->x, I->y, true);
		step--;
		WorldState.pop_back();
		end_of_game = false;
	}
}

void LifeWorld::save(string str)
{
	ofstream outfile(str);
	for (int i = 0; i < WORLD_SIZE; i++)
		for (int j = 0; j < WORLD_SIZE; j++)
			outfile << (curr_ground.GetCell(i, j) ? '1' : '0');
}


void LifeWorld::load(string str)
{
	ifstream infile(str);
	for (int i = 0; i < WORLD_SIZE; i++)
		for (int j = 0; j < WORLD_SIZE; j++)
		{
			char ch;
			bool b = false;
			infile >> ch;
			if (ch == '1') b = true;
			curr_ground.SetCell(i, j, b);
		}
	step = 0;
	WorldState.clear();
	end_of_game = false;
}

LifeWorld::LifeWorld()
{
	step = 0;
	end_of_game = false;
}
LifeWorld::~LifeWorld()
{
}

int main()
{
	LifeWorld lw;
	lw.Draw();
	string option;
	cin >> option;
	while (option != "exit")
	{
		if (option == "reset")
		{
			lw.reset();
			lw.Draw();
		}
		if (option == "set")
		{
			cin >> option;
			lw.setXY(option);
			lw.Draw();
		}
		if (option == "clear")
		{
			cin >> option;
			lw.clearXY(option);
			lw.Draw();
		}
		if (option == "step")
		{
			cin >> option;
			lw.stepN(option);
			lw.Draw();
			if(lw.is_end_of_game()) cout << "End of game" << endl;
		}
		if (option == "back")
		{
			lw.back();
			lw.Draw();
		}
		if (option == "save")
		{
			cin >> option;
			lw.save(option);
			lw.Draw();
		}
		if (option == "load")
		{
			cin >> option;
			lw.load(option);
			lw.Draw();
		}
		cin >> option;
	}
	system("pause");
}
